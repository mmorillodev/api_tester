package com.example.nescara.myapplication.default_folder;

public enum Method {
    GET(0), POST(1), PUT(3), DELETE(2), OPTIONS(4), TRACE(5), DEFAULT(6);

    private int method;

    Method(int method){
        this.method = method;
    }

    public int getCode(){
        return method;
    }
}

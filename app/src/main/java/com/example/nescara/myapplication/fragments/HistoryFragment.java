package com.example.nescara.myapplication.fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nescara.myapplication.adapters.RecyclerViewAdapter;
import com.example.nescara.myapplication.data.Request;
import com.example.nescara.myapplication.database.HistoryDAO;
import com.example.nescara.myapplication.R;
import com.example.nescara.myapplication.interfaces.OnDeleteRequestListener;
import com.example.nescara.myapplication.interfaces.OnRedoRequestListener;

import java.util.ArrayList;
import java.util.List;

public class HistoryFragment extends Fragment implements View.OnScrollChangeListener, OnDeleteRequestListener {
    private RecyclerView recyclerView;
    private RecyclerViewAdapter recyclerViewAdapter;
    private LinearLayoutManager layoutManager;
    private LinearLayout fragmentHistory;
    private View view;
    private TextView txtNoData;
    private SwipeRefreshLayout swipeRefresh;
    private HistoryDAO db;
    List<Request> requests;
    private OnRedoRequestListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(getActivity() instanceof OnRedoRequestListener){
            listener = (OnRedoRequestListener) getActivity();
        }
        else{
            listener = bundle -> {};
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        this.view = view;

        recyclerView = view.findViewById(R.id.recyclerView);
        swipeRefresh = view.findViewById(R.id.refresh);
        fragmentHistory = view.findViewById(R.id.fragment_history);
        txtNoData = view.findViewById(R.id.txtNoData);
        requests = new ArrayList<>();

        db = new HistoryDAO(view.getContext());

        recyclerView.setVisibility(View.VISIBLE);

        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        recyclerViewAdapter = new RecyclerViewAdapter(getActivity(), new ArrayList<>(), listener, this);

        recyclerView.setOnScrollChangeListener(this);

        swipeRefresh.setOnRefreshListener(() -> {
            setHistory();
            Toast.makeText(getContext(), "History successfully updated!", Toast.LENGTH_SHORT).show();
            swipeRefresh.setRefreshing(false);
        });
        setHistory();

        return view;
    }

    public void setHistory(){
        requests = db.read();

        if(requests.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            txtNoData.setVisibility(View.GONE);
            recyclerView.setAdapter(new RecyclerViewAdapter(getActivity(), requests, listener, this));
        }
        else{
            recyclerView.setVisibility(View.GONE);
            txtNoData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean delete(int id){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(true);
        builder.setTitle("Delete data?");
        builder.setPositiveButton("Confirm", (dialog, which) -> {
            db.delete(id);
            setHistory();
        });
        builder.setNegativeButton(android.R.string.cancel, (dialog, which) -> {
            return;
        });
        builder.show();
        return true;
    }

    @Override
    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        View c = recyclerView.getChildAt(0);
        if (c == null) {
            return;
        }
        int scrolly = -c.getTop() + recyclerView.getPaddingTop() + layoutManager.findFirstVisibleItemPosition() * c.getHeight();
        if(scrolly == 0)
            swipeRefresh.setEnabled(true);
        else
            swipeRefresh.setEnabled(false);
    }

    public int getCount(){
        return recyclerView.getChildCount();
    }
}
